const mongoose = require('mongoose')
const crypto = require('crypto')
// const _ = require('lodash')

// 회원 스키마
const User = new mongoose.Schema({
   username: { type: String, required: true },
   id: { type: String, required: true, unique: true },
   password: { type: String, required: true },
   phone: { type: String, required: true, unique: true },
   thumbnail: { type: String, default: 'images/thumbnail/default.png', required: true },
   email: { type: String },
   emailVerifiedAt: { type: Date, default: Date.now },
   rememberToken: { type: String },
   farms: [ String ],
   posts: [ String ],
   // 삭제 여부
   deletionStatus: { type: Boolean, default: false },
   // 일자
   createdAt: { type: Date, default: Date.now },
   updatedAt: { type: Date, default: Date.now },
   deletedAt: { type: Date },
})

const hash = (password) => {
   return crypto.createHmac('sha256', process.env.SECRET_KEY).update(password)
      .digest('hex')
}

User.methods.validatePassword = function(password) {
   // 함수로 전달받은 password 의 해시값과, 데이터에 담겨있는 해시값을 비교
   const hashed = hash(password)
   return this.password === hashed
}

User.statics.auth = async function({ id, password }) {
   return await this.findOne({ id: id, password: hash(password) })
}

// User.statics.findByPhone = function(phone) {
//    return this.findOne({phone: phone}).exec()
// }
// //
// User.statics.findByIdOrPhone = function({id, phone}) {
//    return this.findOne({
//       $or: [{id: id}, {phone}],
//    }).exec()
// }
// 로컬 회원 가입
// User.statics.localRegister = function(reqUser) {
//    // lodash 사용 객체 복사
//    const cloneUser = _.cloneDeep(reqUser)
//    // update 를 이용하여 password hash 변경
//    _.update(cloneUser, 'password', function(pw) {
//       return crypto.createHmac('sha256', process.env.SECRET_KEY).update(pw)
//          .digest('hex')
//    })

//    const user = new this(cloneUser)
//    return user.save()
// }
// //


module.exports = mongoose.model('User', User, 'Users')
