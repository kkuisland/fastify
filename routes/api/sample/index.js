'use strict'

const carController = require(process.env.PWD + '/controllers/carController')
module.exports = async function(fastify, opts) {
   fastify.get('', carController.getCars)
   fastify.get(':id', carController.getStringCar)
   fastify.post('', carController.addCar)
   fastify.put(':id', carController.updateCar)
   fastify.delete(':id', carController.deleteCar)
}
