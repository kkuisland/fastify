'use strict'

const authController = require(process.env.PWD + '/controllers/authController')
module.exports = async function(fastify, opts) {
   fastify.get('', authController.getAuth)
   // fastify.get('/:id', authController.getStringCar)
   fastify.post('', authController.makeAuth)
   // fastify.put('/:id', authController.updateCar)
   fastify.delete(':id', authController.delete)
}
