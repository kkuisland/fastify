'use strict'

const fp = require('fastify-plugin')

/**
 * fastify-corsFastify 애플리케이션에서 CORS 를 사용할 수 있습니다 .
 * Fastify 버전을 지원합니다 3.x. Fastify 호환성 은 이 분기 및 관련 버전을 참조하십시오 ^2.x. Fastify 호환성 은 이 분기 및 관련 버전을 참조하십시오 ^1.x.
 *
 * @see https://github.com/fastify/fastify-cors
 */

module.exports = fp(async function(fastify, opts, next) {
   fastify.register(require('fastify-cors'), {
      origin: true,
      credentials: true,
      methods: [ 'GET', 'POST', 'PUT', 'DELETE', 'OPTIONS' ]
   })

   next()
})
