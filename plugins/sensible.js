'use strict'

const fp = require('fastify-plugin')

/**
 * 이 플러그인은 HTTP 오류를 처리 할 수있는 일부 유틸리티를 추가합니다.
 *
 * @see https://github.com/fastify/fastify-sensible
 */
module.exports = fp(async function(fastify, opts) {
   fastify.register(require('fastify-sensible'), {
      errorHandler: false
   })
})
