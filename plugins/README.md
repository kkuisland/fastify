# Plugins Folder
플러그인은 모든 경로에 공통적 인 동작을 정의합니다.
애플리케이션.인증, 캐싱, 템플릿 및 다른 모든 십자가
절단 우려는이 폴더에 배치 된 플러그인으로 처리해야합니다.

이 폴더의 파일은 일반적으로 다음을 통해 정의됩니다
[`fastify-plugin`](https://github.com/fastify/fastify-plugin) module,
캡슐화되지 않도록하십시오.그들은 데코레이터를 정의하고 후크를 설정할 수 있습니다
그런 다음 나머지 응용 프로그램에서 사용됩니다.

Check out:

* [The hitchhiker's guide to plugins](https://www.fastify.io/docs/latest/Plugins-Guide/)
* [Fastify decorators](https://www.fastify.io/docs/latest/Decorators/).
* [Fastify lifecycle](https://www.fastify.io/docs/latest/Lifecycle/).
