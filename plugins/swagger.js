'use strict'

const fp = require('fastify-plugin')

/**
 * 경로 스키마 또는 기존 Swagger/OpenAPI 스키마에서
 * 자동으로 생성된 Swagger(OpenAPI v2) 또는 OpenAPI v3 스키마를 사용하여
 * Swagger UI 를 제공하기 위한 Fastify 플러그인입니다 .
 *
 * @see https://github.com/fastify/fastify-swagger
 */
module.exports = fp(async function(fastify, opts) {
   fastify.register(require('fastify-swagger'), {
      routePrefix: '/documentation',
      swagger: {
         info: {
            title: 'Test swagger',
            description: 'Testing the Fastify swagger API',
            version: '0.1.0'
         },
         externalDocs: {
            url: 'https://swagger.io',
            description: 'Find more info here'
         },
         host: 'localhost',
         schemes: ['http'],
         consumes: ['application/json'],
         produces: ['application/json'],
         tags: [
            { name: 'user', description: 'User related end-points' },
            { name: 'code', description: 'Code related end-points' }
         ],
         definitions: {
            User: {
               type: 'object',
               required: ['id', 'email'],
               properties: {
                  id: { type: 'string', format: 'uuid' },
                  firstName: { type: 'string' },
                  lastName: { type: 'string' },
                  email: {type: 'string', format: 'email' }
               }
            }
         },
         securityDefinitions: {
            apiKey: {
               type: 'apiKey',
               name: 'apiKey',
               in: 'header'
            }
         }
      },
      uiConfig: {
         docExpansion: 'full',
         deepLinking: false
      },
      staticCSP: true,
      transformStaticCSP: (header) => header,
      exposeRoute: true
   })

   fastify.put('/some-route/:id', {
      schema: {
         description: 'post some data',
         tags: ['user', 'code'],
         summary: 'qwerty',
         params: {
            type: 'object',
            properties: {
               id: {
                  type: 'string',
                  description: 'user id'
               }
            }
         },
         body: {
            type: 'object',
            properties: {
               hello: { type: 'string' },
               obj: {
                  type: 'object',
                  properties: {
                     some: { type: 'string' }
                  }
               }
            }
         },
         response: {
            201: {
               description: 'Successful response',
               type: 'object',
               properties: {
                  hello: { type: 'string' }
               }
            },
            default: {
               description: 'Default response',
               type: 'object',
               properties: {
                  foo: { type: 'string' }
               }
            }
         },
         security: [
            {
               apiKey: []
            }
         ]
      }
   }, (req, reply) => {})

   fastify.ready(err => {
      if(err) throw err
      fastify.swagger()
   })
})
