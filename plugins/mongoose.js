'use strict'

const fp = require('fastify-plugin')
const mongoose = require('mongoose')

/**
 * 몽고 DB
 *
 * @see
 */

module.exports = fp(async function(fastify, opts, next) {
   // fastify.register(require('fastify-formbody'))
   // fastify.mongoose.Promise = global.Promise // Node 의 네이티브 Promise 사용
   mongoose
      .connect(process.env.MONGODB_URL, {
         // useFindAndModify: false,
         useNewUrlParser: true,
         useUnifiedTopology: true,
      })
      .then((response) => {
         console.log('몽고 DB 연결 성공')
      })
      .catch((e) => {
         console.error(e)
      })

   console.log('mongodb connect')
   next()
})
