'use strict'

const fp = require('fastify-plugin')

// Fastify-Plugin의 사용이 필요합니다.
// 데코레이터를 바깥 쪽 범위로 내보낼 수 있습니다

module.exports = fp(async function(fastify, opts) {
   fastify.decorate('someSupport', function() {
      return 'hugs'
   })
})
