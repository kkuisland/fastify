'use strict'

const path = require('path')
const AutoLoad = require('fastify-autoload')

module.exports = async function(fastify, opts) {
   // 사용자 정의 코드를 여기에 놓습니다!

   // 다음 줄을 만지지 마십시오

   // 플러그인에 정의 된 모든 플러그인을로드합니다
   // 그것들은 재사용 된 플러그인을 지원해야합니다
   // 응용 프로그램을 통해
   fastify.register(AutoLoad, {
      dir: path.join(__dirname, 'plugins'),
      options: Object.assign({}, opts),
   })

   // 모든 플러그인을로드합니다
   // 이들 중 하나에서 당신의 노선을 정의하십시오
   fastify.register(AutoLoad, {
      dir: path.join(__dirname, 'routes'),
      options: Object.assign({}, opts),
   })
}
