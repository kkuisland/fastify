'use strict'

// .ENV 파일을 읽습니다.
require('dotenv').config()

// 프레임 워크가 필요합니다
const Fastify = require('fastify')

// 해제 프로세스, 정상적으로 프로세스를 종료하려면 라이브러리가 필요합니다.
const closeWithGrace = require('close-with-grace')

// 일부 구성으로 인스턴스화합니다
const app = Fastify({
   logger: true
})

// 응용 프로그램을 일반 플러그인으로 등록하십시오.
const appService = require('./app.js')
app.register(appService)


// 지연은 완전한 완료에 가까운 우아한 밀리 초 수입니다.
const closeListeners = closeWithGrace({ delay: 500 }, async function({ signal, err, manual }) {
   if(err) {
      app.log.error(err)
   }
   await app.close()
})

app.addHook('onClose', async(instance, done) => {
   closeListeners.uninstall()
   done()
})

// 듣기를 시작합니다.
app.listen(process.env.PORT || 3000, (err) => {
   if(err) {
      app.log.error(err)
      process.exit(1)
   }
})
