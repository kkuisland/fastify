const boom = require('boom')
const Car = require(process.env.PWD + '/models/Car.js')

module.exports = {

   getCars: async(request, reply) => {
      try {
         const cars = await Car.find()
         reply.send(cars)
      } catch (err) {
         throw boom.boomify(err)
      }
   },

   getStringCar: async(request, reply) => {
      try {
         const id  = request.params.id
         const car = await Car.findById(id)
         reply.send(car)
      } catch (err) {
         throw boom.boomify(err)
      }
   },

   addCar: async(request, reply, next) => {
      try {
         const car = await new Car(request.body)
         reply.send(await car.save())
      } catch (err) {
         throw boom.boomify(err)
      }
   },

   updateCar: async(request, reply) => {
      try {
         const id = request.params.id
         const car = request.body
         const { ...updateData } = car
         console.log('car =>', car)
         console.log('updateData =>', updateData)
         const update = await Car.findByIdAndUpdate(id, updateData, { new: true })
         reply.send(update)
      } catch (err) {
         throw boom.boomify(err)
      }
   },

   deleteCar: async(request, reply) => {
      const { id } = request.query
      let car = null

      try {
         car = await Car.findByIdAndRemove(id)
      } catch (err) {
         throw boom.boomify(err)
      }

      if (!car) {
         reply.code(404).send({ message: '자동차 정보를 찾을 수 없습니다.' })
      }

      reply.code(201).send({ car: car, message: '자동차 제거 성공' })
   },

}
