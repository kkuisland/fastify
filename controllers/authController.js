const boom = require('boom')
const Auth = require(process.env.PWD + '/models/User.js')

module.exports = {

   getAuth: async(request, reply) => {
      let token = null
      try {
         token = await request.unsignCookie(request.cookies.kkuToken)
      } catch (err) {
         boom.boomify(err)
      }
      console.log('token => ', token)

      if (!token) {
         reply.code(404).send({ message: '쿠키 정보 없음!' })
      }

      let decoded = null
      try {
         decoded = await request.jwtVerify(token.value)
      } catch (err) {
         throw await boom.boomify(err, { message: '저장된 정보가 없습니다' })
      }
      reply.send(decoded)
   },

   makeAuth: async(request, reply) => {
      const { ...reqData } = request.body
      let user = null
      try {
         user = await Auth.auth(reqData)
      } catch (err) {
         throw await boom.boomify(err, { message: '잘못된 사용자 정보' })
      }

      if (!user) {
         reply.code(404).send({ message: '사용자 정보를 찾을 수 없습니다.' })
      }

      // 토큰 생성
      const token = await reply.jwtSign({
         id: user.id,
         phone: user.phone,
         thumbnail: user.thumbnail,
         email: user.email,
         emailVerifiedAt: user.emailVerifiedAt,
         rememberToken: user.rememberToken,
         farms: user.farms,
         posts: user.posts,
         role: [ 'admin', 'main' ],
         company: 'KKu Island'
      })

      // 쿠키 생성
      await reply.setCookie('kkuToken', token, {
         //domain: *,
         path: '/',
         httpOnly: true,
         secure: true,
         sameSite: 'None',
         overwrite: true,
         maxAge: 24 * 60 * 60 * 5,
         signed: true
      }).code(200)
         .send({
            id: user.id,
            phone: user.phone,
            thumbnail: user.thumbnail,
            email: user.email,
            emailVerifiedAt: user.emailVerifiedAt,
            rememberToken: user.rememberToken,
            farms: user.farms,
            posts: user.posts,
            role: [ 'admin', 'main' ],
            company: 'KKu Island'
         })

      // 결과 전송
      // reply.send(user)
   },

   delete: async(request, reply) => {
      // const { id } = request.query
      // const token = await request.unsignCookie(request.cookies.kkuToken)
      // 쿠키 제거
      await reply.setCookie('kkuToken', '', {
         //domain: *,
         path: '/',
         httpOnly: true,
         secure: true,
         sameSite: 'None',
         overwrite: true,
         maxAge: 0,
         signed: true
      }).code(200)
         .send({
            message: '쿠키 제거'
         })
   }
}
